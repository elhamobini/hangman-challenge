
const button = document.getElementById("start");
const timeH = document.querySelector('h6');
let timeSecond = 120;

button.onclick = () => {
    const countDown = setInterval(() => {
        timeSecond--;
        displayTime(timeSecond);
        if (timeSecond <= 0 || timeSecond < 1){
            endTime();
            clearInterval(countDown);
        }
    }, 1000);
    document.querySelector("#letters").style.opacity= "100%";

}

displayTime(timeSecond);


function displayTime(second){
    const min = Math.floor(second / 60);
    const sec = Math.floor(second % 60);
    timeH.innerHTML = `${min <10 ? '0' : ''}${min}:${sec< 10 ? '0': ''}${sec}`
}
 function endTime(){
     timeH.innerHTML = "Time OUT";
     document.querySelector("#letters").style.opacity= "10%";
     
 }


const secretPhrases = ["never", "yourself", "that", "bullet", "breakup",];


let randomItem = "";
let clicked = [];
let result = "";
let mistakes = 0;

function selectRandomItem(){
    randomItem = secretPhrases[Math.floor(Math.random() * secretPhrases.length  )];
    document.getElementById("letters").addEventListener("click" , buttonHandler);
    window.addEventListener("keydown", keyHandler);
    console.log(randomItem)
}

function setUnderScores() {
    let splitedWord = randomItem.split("");
    let mappedWord = splitedWord.map(letter => clicked.indexOf(letter) >= 0 ? letter : "_");
    result = mappedWord.join("");
    document.getElementById("clue").innerHTML = `<p>${result}</p>`
}

function checkIfWon() {
    if (randomItem === result){
        document.getElementById("gameover").querySelector("p").style.display = "block";
        document.getElementById("image").querySelector("img").src = "assets/winner-png"
    }
}

function checkIfLose(){
    if(mistakes === 10){
        document.getElementById("gameover").querySelector("p").style.display = "block";
        document.getElementById("clue").querySelector("p").innerText = `Random Word Is ${randomItem}`;
    }
}

function updateHangmanImg(){
    const image = document.getElementById("image").querySelector("img");
    image.src = `assets/hm${mistakes}.gif`
}

function letterHandler(letter){
    letter = letter.toLowerCase();
    clicked.indexOf(letter) === -1 ? clicked.push(letter) : null ;
    document.getElementById(letter.toUpperCase()).className = "used";

    if (randomItem.indexOf(letter) >= 0){
        setUnderScores();
        checkIfWon();
    } else if (randomItem.indexOf(letter) === -1){
        mistakes++;
        checkIfLose();
        updateHangmanImg();
    }
}

function buttonHandler(event){
    letterHandler(event.target.id);
}
function keyHandler(event){
    letterHandler(event.key);
}




selectRandomItem();
setUnderScores();